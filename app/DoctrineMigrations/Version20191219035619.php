<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191219035619 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25B88FF97F');
        $this->addSql('DROP INDEX UNIQ_527EDB25B88FF97F ON task');
        $this->addSql('ALTER TABLE task CHANGE task_user_id user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB258D93D649 FOREIGN KEY (user) REFERENCES `user` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_527EDB258D93D649 ON task (user)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB258D93D649');
        $this->addSql('DROP INDEX UNIQ_527EDB258D93D649 ON task');
        $this->addSql('ALTER TABLE task CHANGE user task_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25B88FF97F FOREIGN KEY (task_user_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_527EDB25B88FF97F ON task (task_user_id)');
    }
}
