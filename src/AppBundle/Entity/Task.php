<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Task.
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
 */
class Task
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     *
     * @Assert\NotBlank
     * @ORM\Column(name="task", type="string", length=255)
     */
    private $task;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dueDate", type="date")
     */
    private $dueDate;
    // /**
    //  * @Assert\Callback
    //  */
    // public function validate(ExecutionContextInterface $context, $payload)
    // {
    //     if ($this->getTask()== 'asdddd') {
    //         $context->buildViolation('bawal')
    //             ->addViolation();
    //     }
    // }
    /**
     * @Assert\IsFalse(message="The token is invalid.")
     * @Assert\IsTrue(message="The token is Valid.")
     */
    // public function isTokenValid()
    // {
    //     return true;
    // }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set task.
     *
     * @param string $task
     *
     * @return Task
     */
    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task.
     *
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get task.
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dueDate.
     *
     * @param \DateType $dueDate
     *
     * @return Task
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate.
     *
     * @return \DateType
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }
}
